package org.gcube.data.spd.wormsplugin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import org.gcube.data.spd.model.CommonName;
import org.gcube.data.spd.model.products.Taxon;
import org.gcube.data.spd.model.products.TaxonomyItem;
import org.gcube.data.spd.model.products.TaxonomyStatus;
import org.gcube.data.spd.model.products.TaxonomyStatus.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_jena.cs.fusion.client.worms.AphiaRecord;
import de.uni_jena.cs.fusion.client.worms.Classification;
import de.uni_jena.cs.fusion.client.worms.Vernacular;
import de.uni_jena.cs.fusion.client.worms.WormsClientException;


public class Utils {

	static Logger logger = LoggerFactory.getLogger(Utils.class);

	public static Taxon retrieveTaxon(Classification classification, long aphiaID){
		Taxon taxon = null;
		while (classification.aphiaId!=aphiaID){
			Taxon newTaxon = new Taxon(classification.aphiaId+"");
			if (taxon!=null) 
				newTaxon.setParent(taxon);
			newTaxon.setRank(classification.taxonomicRank);
			//			logger.trace(classification.getRank());
			//			logger.trace(classification.getScientificname());
			newTaxon.setScientificName(classification.scientificName);
			classification = classification.child;
			taxon = newTaxon;
		}
		//		logger.trace(taxon);
		return taxon;
	}


	public static TaxonomyItem retrieveTaxonomy(Classification classification, long aphiaID){
		List<CommonName> listCommNames = new ArrayList<CommonName> ();
		TaxonomyItem taxon = null;

		while (classification.aphiaId!=aphiaID){

			int id = classification.aphiaId;
			TaxonomyItem newTaxon = new TaxonomyItem(id+"");
			if (taxon!=null) 
				newTaxon.setParent(taxon);
			
			newTaxon.setRank(classification.taxonomicRank);
			newTaxon.setScientificName(classification.scientificName);
			newTaxon.setCredits(Utils.createCredits());			
			AphiaRecord record = null;

			try {
				record = WormsPlugin.wormsClient.aphiaRecordByAphiaId(id);
			}catch (Exception e) {
				logger.error("Error getAphiaRecordByID ", e);
			}
			if (record != null){
				newTaxon.setCitation(record.citation);
				newTaxon.setLsid(record.lsid);
				newTaxon.setScientificNameAuthorship(record.authority);
			}

			Collection<Vernacular> vernaculars;
			try {
				vernaculars = WormsPlugin.wormsClient.aphiaVernacularsByAphiaId(classification.aphiaId);
				if (vernaculars!=null){
					//					logger.debug("found vernacular name");
					for (Vernacular vernacular : vernaculars) {										
						if (vernacular.languageCode!=null){
							CommonName a = new CommonName(vernacular.language,vernacular.vernacular);
							listCommNames.add(a);		
						}
					}
				}
			} catch (WormsClientException e) {
				logger.error("RemoteException", e);
			}

			newTaxon.setCommonNames(listCommNames);
			newTaxon.setStatus(new TaxonomyStatus("accepted", Status.ACCEPTED));
			classification = classification.child;
			taxon = newTaxon;
		}
		return taxon;
	}

	//format date
	public static String createDate() {
		Calendar now = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String date = format.format(now.getTime());
		return date;
	}

	public static String createCredits() {
		String cred = WormsPlugin.credits;
		cred = cred.replace("XDATEX",createDate());	
		return cred;
	}
}
