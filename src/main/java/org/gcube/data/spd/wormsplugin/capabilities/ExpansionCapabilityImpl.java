package org.gcube.data.spd.wormsplugin.capabilities;

import java.util.Collection;

import org.gcube.data.spd.model.exceptions.ExternalRepositoryException;
import org.gcube.data.spd.model.exceptions.StreamNonBlockingException;
import org.gcube.data.spd.plugin.fwk.capabilities.ExpansionCapability;
import org.gcube.data.spd.plugin.fwk.writers.ObjectWriter;
import org.gcube.data.spd.wormsplugin.WormsPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_jena.cs.fusion.client.worms.AphiaRecord;
import de.uni_jena.cs.fusion.client.worms.WormsClientException;

public class ExpansionCapabilityImpl implements ExpansionCapability {

	Logger logger = LoggerFactory.getLogger(ExpansionCapabilityImpl.class);
	
	@Override
	public void getSynonyms(ObjectWriter<String> writer, String scientifcName) throws ExternalRepositoryException {

		logger.debug("searching synonyms for "+scientifcName);
		int offset =1;
		int elements =20;
		Collection<AphiaRecord> records = null;
		do{
			try{			
				records = WormsPlugin.wormsClient.aphiaRecordsByName(scientifcName, true, false, offset);
				if (records!=null){
					for (AphiaRecord record : records){
						try{	
							Collection<AphiaRecord> synonyms = WormsPlugin.wormsClient.aphiaSynonymsByAphiaId(record.aphiaId);
							
							if (synonyms!=null){
								for (AphiaRecord synonym: synonyms){
									if (!writer.isAlive()) return;
									writer.write(synonym.scientificName);
									logger.debug("found synonym "+synonym.scientificName);
								}
							}
						}catch (Exception e) {
							writer.write(new StreamNonBlockingException("WoRMS",scientifcName));
							logger.error("error retrieving synonyms for aphia id "+record.aphiaId, e);
						}
					}
				}
			} catch (WormsClientException e) {
				throw new ExternalRepositoryException(e);
			}
			offset = elements+offset;
		}while(records!=null && records.size() == offset );
	}

}
