package org.gcube.data.spd.wormsplugin.capabilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.gcube.data.spd.model.CommonName;
import org.gcube.data.spd.model.Condition;
import org.gcube.data.spd.model.Conditions;
import org.gcube.data.spd.model.exceptions.ExternalRepositoryException;
import org.gcube.data.spd.model.exceptions.IdNotValidException;
import org.gcube.data.spd.model.exceptions.MethodNotSupportedException;
import org.gcube.data.spd.model.products.TaxonomyItem;
import org.gcube.data.spd.model.products.TaxonomyStatus;
import org.gcube.data.spd.model.products.TaxonomyStatus.Status;
import org.gcube.data.spd.model.util.ElementProperty;
import org.gcube.data.spd.plugin.fwk.capabilities.ClassificationCapability;
import org.gcube.data.spd.plugin.fwk.writers.ClosableWriter;
import org.gcube.data.spd.plugin.fwk.writers.ObjectWriter;
import org.gcube.data.spd.wormsplugin.Utils;
import org.gcube.data.spd.wormsplugin.WormsPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_jena.cs.fusion.client.worms.AphiaRecord;
import de.uni_jena.cs.fusion.client.worms.Source;
import de.uni_jena.cs.fusion.client.worms.Vernacular;
import de.uni_jena.cs.fusion.client.worms.WormsClientException;


public class ClassificationCapabilityImpl extends ClassificationCapability {

	
	public enum RecordType {
		SCIENTIFIC,
		VERNACULAR
	}
	
	private static Logger logger = LoggerFactory.getLogger(ClassificationCapabilityImpl.class);

	@Override
	public Set<Conditions> getSupportedProperties() {
		return Collections.emptySet();
	}

	@Override
	public List<TaxonomyItem> retrieveTaxonChildrenByTaxonId(String id) throws IdNotValidException, ExternalRepositoryException {

		//		logger.trace(id);
		List<TaxonomyItem> list = new ArrayList<TaxonomyItem>(); 	
		try {
			Collection<AphiaRecord> records;
			final int offsetlimit=50;
			int offset =1;
			do{
				records =  WormsPlugin.wormsClient.aphiaChildrenByAphiaId(Long.parseLong(id), false, offset);

				if (records!=null){
					//										logger.debug(records.length);
					for (AphiaRecord record : records){
						TaxonomyItem item = createItem(record, false);
						if (item!=null)
							list.add(item);
					}
				}
				offset+=offsetlimit;
			} while (records!=null && records.size()==offsetlimit);
		}catch (NumberFormatException e) {
			logger.error("id not valid",e);
			throw new IdNotValidException(e);
		} catch (Throwable e) {
			logger.error("error contacting worms service", e);
			throw new ExternalRepositoryException(e);
		}

		return list;
	}


	public void retrieveTaxa(ObjectWriter<TaxonomyItem> writer, String word, RecordType type) throws ExternalRepositoryException{

		Set<Long> hash = new HashSet<Long>();
 		
		try {
			Collection<AphiaRecord> records = null;

			final int offsetlimit=50;
			int offset =1;
			do{
				switch (type) {
				case SCIENTIFIC:
					records = WormsPlugin.wormsClient.aphiaRecordsByName(word, true, false, offset);
					break;
				case VERNACULAR:
					records = WormsPlugin.wormsClient.aphiaRecordsByVernacular(word, true, offset);
					break;	
				default:
					logger.warn("invalid record type");
					break;
				}
				
				
				if (records!=null){
					for (AphiaRecord record : records){
						if (hash.contains(record.aphiaId)){
							continue;
						}

						hash.add(record.aphiaId);
						
						TaxonomyItem item = createItem(record, true);
						if ((item != null) && (writer.isAlive()))
							writer.write(item);
						

					}
				}


				offset+=offsetlimit;
			} while (records!=null && records.size()==offsetlimit);

		} catch (Throwable e) {
			logger.error("error contacting worms service", e);
			throw new ExternalRepositoryException(e);
		}
		
	}

	//create TaxonomyItem
	private TaxonomyItem createItem(AphiaRecord record, Boolean flag) throws WormsClientException {
		TaxonomyItem item = null;
		String scientificname = record.scientificName;
		//		logger.trace("scientificname " + scientificname);
		if (scientificname != null ){

			item = new TaxonomyItem(record.aphiaId+"");

			item.setScientificName(record.scientificName);
			item.setScientificNameAuthorship(record.authority);
			item.setLsid(record.lsid);

			item.setCredits(Utils.createCredits());			
			item.setCitation(record.citation);

			if (record.taxonomicRank!=null)
				item.setRank(record.taxonomicRank);
			else if (record.scientificName.equals("Biota"))
				item.setRank("Superdomain");

			List<CommonName> listCommNames = new ArrayList<CommonName> ();

			Collection<Vernacular> vernaculars = WormsPlugin.wormsClient.aphiaVernacularsByAphiaId(record.aphiaId);
			if (vernaculars!=null){
				for (Vernacular vernacular : vernaculars) {			
					if (vernacular.languageCode!=null){
						CommonName a = new CommonName(vernacular.language,vernacular.vernacular);
						listCommNames.add(a);		
					}
				}
			}
			item.setCommonNames(listCommNames);		

			try{
				if (record.status.equals("accepted"))
					item.setStatus(new TaxonomyStatus("accepted", Status.ACCEPTED));
				else if (record.status.equals("unaccepted") && WormsPlugin.wormsClient.aphiaRecordByAphiaId(record.validAphiaId).status.equals("accepted")){
					//							logger.trace(WormsPlugin.binding.getAphiaSynonymsByID(record.getValid_AphiaID()));						
					item.setStatus(new TaxonomyStatus(Status.SYNONYM, record.validAphiaId+"", "synonym"));}
				else
					item.setStatus(new TaxonomyStatus(record.status, Status.UNKNOWN));
			}catch (Exception e) {
				item.setStatus(new TaxonomyStatus(record.status, Status.UNKNOWN));
			}

			if (flag){
				try{
					item.setParent(Utils.retrieveTaxonomy( WormsPlugin.wormsClient.aphiaClassificationByAphiaId(record.aphiaId), record.aphiaId));
				}catch (Exception e) {
					item.setParent(null);
				}
			}
			else 
				item.setParent(null);
			
			Collection<Source> sources = null;
			if ((sources= WormsPlugin.wormsClient.aphiaSourcesByAphiaId(record.aphiaId))!=null){
				for (Source source : sources){
					//			logger.trace(source.getReference());
					if (source==null)
						continue;
					if (source.reference!=null){

						StringBuilder p = new StringBuilder();
						p.append(source.reference);
						if (source.link!=null){
							p.append(", available online at ");
							p.append(source.link);
						}
						if (source.url!=null){
							p.append(", details: ");
							p.append(source.url);
						}				
						ElementProperty property = new ElementProperty(source.use, p.toString());
						item.addProperty(property);				
					}
				}
			}
		}

		return item;
	}

		

	@Override
	public void retrieveTaxonByIds(Iterator<String> ids, ClosableWriter<TaxonomyItem> writer) throws ExternalRepositoryException {

		try{
			while(ids.hasNext()) {

				String id = ids.next(); 
				//				logger.trace("Retrive taxon by id " + id);
				TaxonomyItem tax = retrieveTaxonById(id);
				if ((tax != null) && (writer.isAlive()))
					writer.write(tax);
				else
					break;
			}

		} catch (IdNotValidException e) {
			logger.error("IdNotValidn", e);
		} catch (ExternalRepositoryException e) {
			logger.error("ExternalRepositoryException", e);
			throw e;
		}finally{
			writer.close();
		}
	}


	@Override
	public TaxonomyItem retrieveTaxonById(String id)
			throws IdNotValidException, ExternalRepositoryException {

		TaxonomyItem tax = null;
		//		logger.trace("Retrive taxon by id " + id);
		AphiaRecord record = null;

		try {
			record = WormsPlugin.wormsClient.aphiaRecordByAphiaId(Long.parseLong(id));
			if (record!=null){				
				tax = createItem(record, true);
			}
		} catch (NumberFormatException e) {
			logger.error("NumberFormatException", e);
			throw new IdNotValidException(e);
		} catch (WormsClientException e) {
			logger.error("Remote Error", e);
			throw new ExternalRepositoryException(e);
		}

		return tax;

	}


	@Override
	public void getSynonymnsById(ObjectWriter<TaxonomyItem> writer, String id)
			throws IdNotValidException, MethodNotSupportedException, ExternalRepositoryException {
		try{			

			Collection<AphiaRecord> records = null;
			if ((records = WormsPlugin.wormsClient.aphiaSynonymsByAphiaId(Long.parseLong(id)))!=null){
				for (AphiaRecord record : records){

					TaxonomyItem tax = createItem(record, true);
					if ((tax != null) && (writer.isAlive()))
						writer.write(tax);
					else
						break;
				}	
			}
		}catch (Exception e) {
			logger.error("General Error", e);
			throw new ExternalRepositoryException(e);
		}
	}

	@Override
	public void searchByScientificName(String word,
			ObjectWriter<TaxonomyItem> writer, Condition... properties) throws ExternalRepositoryException{
		retrieveTaxa(writer, word, RecordType.SCIENTIFIC);
		
	}
	
}
