package org.gcube.data.spd.wormsplugin.capabilities;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.gcube.data.spd.model.exceptions.ExternalRepositoryException;
import org.gcube.data.spd.plugin.fwk.capabilities.MappingCapability;
import org.gcube.data.spd.plugin.fwk.writers.ObjectWriter;
import org.gcube.data.spd.wormsplugin.WormsPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_jena.cs.fusion.client.worms.AphiaRecord;
import de.uni_jena.cs.fusion.client.worms.WormsClientException;

public class NamesMappingImpl implements MappingCapability{

	private static Logger logger = LoggerFactory.getLogger(NamesMappingImpl.class);

	
	@Override
	public void getRelatedScientificNames(ObjectWriter<String> writer,
			String commonName) throws ExternalRepositoryException{
		try {
			logger.debug("retrieving mapping for "+commonName);
			Collection<AphiaRecord> records;
			final int offsetlimit=50;
			int offset =1;
			Set<String> snSet = new HashSet<String>();
			do{
				records = WormsPlugin.wormsClient.aphiaRecordsByVernacular(commonName, true, offset);
				if (records!=null){
					for (AphiaRecord record : records){
						if (!writer.isAlive()) return;
						if (!snSet.contains(record.scientificName)){
							logger.trace("writing (COMMONNAMESMAPPING) "+record.scientificName );
							writer.write(record.scientificName);
							snSet.add(record.scientificName);
						}
					}
				}
				offset+=offsetlimit;
			} while (records!=null && records.size()==offsetlimit);
			
		} catch (WormsClientException e) {
			throw new ExternalRepositoryException(e);
		}
	}



}
